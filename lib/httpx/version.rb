# frozen_string_literal: true

module HTTPX
  VERSION = "0.21.0"
end
